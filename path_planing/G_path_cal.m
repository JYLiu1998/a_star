function closeList_path=G_path_cal(map,a,b)
tic;
%% Greedy  
%初始化起始点、障碍物点
start_node=map(1,:);
target_node=map(end,:);
obs=[];
for i=1:size(map(:,1))-2
    obs(i,:)=map(i+1,:);
end
%初始化closelist
closeList=start_node;
closeList_cost=0;
closeList_path={start_node,start_node};
%寻找子节点
child_nodes= child_nodes_cal(start_node, a, b, obs, closeList); 
%扩展过的子节点个数
child_num=0;
child_num=child_num+size(child_nodes,1);
%初始化openlist
openList=child_nodes;
for i=1:size(openList,1)
    openList_path{i,1}=openList(i,:);
    openList_path{i,2}=[start_node;openList(i,:)];
end
for i=1:size(openList,1)
    %h=abs(openList(i,1)-target_node(1))+abs(openList(i,2)-target_node(2));
    h=norm(openList(i,:)-target_node);
    openList_cost(i)=h;
    %openList_cost(i)=abs(openList(i,1)-target_node(1))+abs(openList(i,2)-target_node(2));
end
%开始搜索
[~,min_idx]=min(openList_cost(:));
parent_node=openList(min_idx,:);
g=norm(parent_node-start_node);

%循环搜索
loop_flag=1;
while loop_flag
    %找父节点的子节点
    child_nodes= child_nodes_cal(parent_node, a, b, obs, closeList);
    child_num=child_num+size(child_nodes,1);
%     for i=1:size(child_nodes,1)
%         child_node=child_nodes(i,:);
%         openList(end+1,:)=child_node;
%         openList_cost(end+1)=abs(child_node(1)-target_node(1))+abs(child_node(2)-target_node(2));
%         openList_path{end+1,1}=child_node;
%         openList_path{end,2}=[openList_path{min_idx,2};child_node];
%     end
     for i=1:size(child_nodes,1)
        child_node=child_nodes(i,:);
        [in_flag,in_idx]=ismember(child_node,openList,'rows');
        %g=openList_cost(min_idx,1)+norm(parent_node-child_node);
      
        %h=abs(child_node(1)-target_node(1))+abs(child_node(2)-target_node(2));
        h=norm(child_node-target_node);
     
        %如果子节点在openlist中，则更新比较带价值
        if in_flag==0
                %openList_path{in_idx,2}=[openList_path{min_idx,2};child_node];
        %如果子节点不在openlist中，则直接添加到openlist中    
            child_num=child_num+1;
            openList(end+1,:)=child_node;
            openList_cost(end+1)=h;
            openList_path{end+1,1}=child_node;
            openList_path{end,2}=[openList_path{min_idx,2};child_node];
         end
       end
    %父节点已经搜索完子节点，因此没有了利用价值，所以需要将父节点从openlist移除到clostlist中
    closeList(end+1,:)=openList(min_idx,:);
    closeList_cost(end+1,:)=openList_cost(min_idx);  %!!!!!!closeList_cost只记录f值即可
    closeList_path(end+1,:) = openList_path(min_idx,:);
%     closeList_path{end+1,1}=openList_path{min_idx,1};   %可以看出每次Closelist_path是增加1个节点的路径
%     closeList_path{end,2}=openList_path{min_idx,2};  

    %将最小代价的父节点清空
    openList(min_idx,:)=[];
    openList_cost(min_idx)=[];
    openList_path(min_idx,:) = [];
    %重新搜索代价最小的点
    [~,min_idx]=min(openList_cost(:));
    g=g+norm(openList(min_idx,:)-parent_node);
    parent_node=openList(min_idx,:);
   %判断是否到达目标节点
   if parent_node==target_node
       closeList(end+1,:)=parent_node;
       closeList_cost(end+1,:)=openList_cost(min_idx);
       closeList_path(end+1,:) = openList_path(min_idx,:);
       loop_flag=0;
   end   
end

path_num=closeList_path{end,2};
step_num=size(path_num,1);
cost=closeList_cost(end);
toc;
sprintf('贪心算法迭代步数为 %d,运行时间为 %0.4f, 最优路径代价为 %0.4f, 一共扩展过的子节点个数为 %d',step_num,toc,g,child_num)

