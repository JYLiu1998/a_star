function map = map_cal(a,b,l,I)
%障碍物赋值
obs_value=55;
I = rgb2gray(I);     %将图片转为灰度图
figure(1)
imshow(I)
I(:,:)=flipud(I(:,:));  %将图片反转
I=I';
B = imresize(I,[a/l b/l]);%   将数字矩阵转为规定的大小
J=floor(B/200);     %将障碍物所在矩阵转为0，其余区域为1.也可自己将其改为障碍物为1，可行域为0.
%% 寻找障碍物，并贴标签
%先纵向依次查找障碍物（每10个一次查找），并赋予相应坐标障碍物标签
for i=1:a/l
    for j=1:1/l:b/l      %只能到91
        count_obs1=sum(J(i,j:j+1/l-1)==0);
        if(count_obs1>=5)
            J((floor(i/10)+1)*10,j+10-1)=obs_value;
        end
    end
end

%横向依次查找障碍物（每10个一次查找），并赋予相应坐标障碍物标签
for j=1:b/l
    for i=1:1/l:a/l   %只能到91
        count_pbs2=sum(J(i:i+1/l-1,j)==0);
        if(count_pbs2>=5)
            J(i+10-1,(floor(j/10)+1)*10)=obs_value;
        end
    end
end

%% 赋值障碍物地图
map = [];
map(1,1) = 1;  %之后定义Xstar 、Ystar
map(1,2) = 1;
k=2;
for i=1/l:1/l:a/l
    for j=1/l:1/l:b/l
        if(J(i,j)==obs_value)
            map(k,1)=floor(i/10);
            map(k,2)=floor(j/10);
            k=k+1;
        end
    end
end
map(k,1)=a;
map(k,2)=b;

