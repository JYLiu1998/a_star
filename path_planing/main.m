clc
clear all
%计算一下扩展节点的个数

%% 读取图片
I=imread('003.png');   %读入图片
a=10; 
b=10; 
l=0.1;    %网格边长
% 栅格地图计算
map=map_cal(a,b,l,I);
%% 开始画图
figure(2)
for obs_cnt = 2: size(map, 1) - 1
    scatter(map(obs_cnt, 1)-0.5,map(obs_cnt, 2)-0.5,250,155,'filled');
    hold on;
    grid on;
    %grid minor;
    %axis equal;        
    axis ([0 a 0 b ]);
    hold on;
end
% start pointG_path
scatter(map(1, 1)-0.5, map(1, 2)-0.5,'b','*');
hold on;
% target point
scatter(map(size(map, 1), 1)-0.5, map(size(map, 1), 2)-0.5, 'r','*');
hold on;

%贪心算法
G_path=G_path_cal(map,a,b);

% Dijikstra
D_path=D_path_cal(map,a,b);


% A*
A_path=A_path_cal(map,a,b);



%% 画路径
path_opt_G=G_path{end,2};
path_opt_G(:,1)=path_opt_G(:,1)-0.5;
path_opt_G(:,2)=path_opt_G(:,2)-0.5;
scatter(path_opt_G(:,1),path_opt_G(:,2),'b');
plot(path_opt_G(:,1),path_opt_G(:,2),'b');
hold on

path_opt_D=D_path{end,2};
path_opt_D(:,1)=path_opt_D(:,1)-0.5;
path_opt_D(:,2)=path_opt_D(:,2)-0.5;
scatter(path_opt_D(:,1),path_opt_D(:,2),'k');
plot(path_opt_D(:,1),path_opt_D(:,2),'k');
hold on

path_opt_A=A_path{end,2};
path_opt_A(:,1)=path_opt_A(:,1)-0.5;
path_opt_A(:,2)=path_opt_A(:,2)-0.5;
scatter(path_opt_A(:,1),path_opt_A(:,2),'r');
plot(path_opt_A(:,1),path_opt_A(:,2),'r');


            
         
            
            



            

            
